import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Usersa } from './users/user.entity';
import { PhotosModule } from './photos/photos.module';
import { UsersModule } from './users/users.module';
@Module({
  imports: [TypeOrmModule.forRoot(), UsersModule],
})
export class AppModule {}

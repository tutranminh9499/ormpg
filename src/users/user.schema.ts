import { EntitySchema } from 'typeorm';
import { Usersa } from './user.entity';

import { from } from 'rxjs';
export const UserSchema = new EntitySchema<Usersa>({
  name: 'Usersa',
  target: Usersa,
  columns: {
    id: {
      type: Number,
    },
    first_name: {
      type: String,
    },
    last_name: {
      type: String,
    },
  },
  relations: {
    // photos: {
    //   type: 'one-to-many',
    //   target: 'Photo', // the name of the PhotoSchema
    // },
  },
});

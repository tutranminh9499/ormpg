import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
// import { InjectModel } from '@nestjs/sequelize';
// import { Sequelize } from 'sequelize-typescript';
import { InjectRepository } from '@nestjs/typeorm';
import {
  getConnection,
  getRepository,
  Repository,
  createConnections,
  Connection,
} from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { Usersa } from './user.entity';
// const connection = await createConnection({
//   type: "mysql",
//   host: "localhost",
//   port: 3306,
//   username: "test",
//   password: "test",
//   database: "test"
// });
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Usersa)
    private usersRepository: Repository<Usersa>,
  ) {}

  async create(createUserDto: CreateUserDto): Promise<Usersa> {
    return await this.usersRepository.save(createUserDto);
    // console.log(user);
  }

  findAll(): Promise<Usersa[]> {
    return getConnection().manager.find(Usersa);
  }

  findOne(id: number): Promise<Usersa> {
    return getRepository(Usersa).findOne(id);
  }

  async remove(id: string): Promise<boolean> {
    if (await this.usersRepository.delete(id)) {
      return true;
    } else return false;
  }

  async update(id: number, updateUserDto: CreateUserDto) {
    if (await this.usersRepository.update(id, updateUserDto)) {
      return true;
    } else return false;
  }
}

create table newuserdata (
	id SERIAL,
	first_name VARCHAR(50),
	last_name VARCHAR(50),
	email VARCHAR(50),
	gender VARCHAR(50),
	ip_address VARCHAR(20),
	avatar VARCHAR(250)
);
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Tana', 'Mighele', 'tmighele0@sohu.com', 'Female', '247.92.40.163','https://s3.amazonaws.com/uifaces/faces/twitter/calebogden/128.jpg');
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Boyd', 'Bernasek', 'bbernasek1@youtube.com', 'Male', '2.95.111.253','https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg');
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Angelica', 'McKnockiter', 'amcknockiter2@aboutads.info', 'Female', '45.210.153.238','https://s3.amazonaws.com/uifaces/faces/twitter/olegpogodaev/128.jpg');
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Arliene', 'Dunton', 'adunton3@uol.com.br', 'Female', '148.206.5.230','https://s3.amazonaws.com/uifaces/faces/twitter/marcoramires/128.jpg');
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Gunar', 'Bartolomeoni', 'gbartolomeoni4@cdbaby.com', 'Male', '151.73.172.102','https://s3.amazonaws.com/uifaces/faces/twitter/stephenmoon/128.jpg');
insert into newuserdata ( first_name, last_name, email, gender, ip_address, avatar) values ('Cammy', 'Barnaby', 'cbarnaby5@elpais.com', 'Female', '94.60.119.248','https://s3.amazonaws.com/uifaces/faces/twitter/bigmancho/128.jpg');

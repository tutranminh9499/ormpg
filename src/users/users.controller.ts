import {
  Controller,
  Get,
  Put,
  Post,
  Delete,
  Body,
  Param,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { getConnection, getManager, getRepository } from 'typeorm';
import { UsersService } from './users.service';
import { Usersa } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';

@Controller('/api/users')
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Post()
  create(@Body() createUserDto: any): Promise<Usersa> {
    var user;
    if(user= this.usersService.create(createUserDto.user)){
      return user;
    }
    else throw new HttpException("Cannot create new user!",HttpStatus.BAD_REQUEST);
  }
  @Put('/:id')
  async update(
    @Param('id') userId: number,
    @Body() createUserDto: any,
  ) {
    if (await this.usersService.update(userId, createUserDto.user)) {  
      throw new HttpException('Update successfully!', HttpStatus.OK);
    } else
      throw new HttpException(
        'Error! Cannot update user!',
        HttpStatus.BAD_REQUEST,
      );
  }

  @Get()
  async getAll() {
    var user;
    if(user= await this.usersService.findAll()){
      return user;
    }
    else throw new HttpException("Cannot find the user required!",HttpStatus.BAD_REQUEST);
  }

  @Get('/:id')
  async getOne(@Param('id') userId: number) {
    var user;
    if(user= await this.usersService.findOne(userId)){
      return user;
    }
    else throw new HttpException("Cannot find the user required!",HttpStatus.BAD_REQUEST);
  }
  @Delete('/:id')
  remove(@Param('id') id: string){
    if(this.usersService.remove(id)){
      throw new HttpException("Delete successfully!",HttpStatus.OK);
    }
    else throw new HttpException("Cannot delete!", HttpStatus.BAD_REQUEST);

  }
}

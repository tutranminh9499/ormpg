import {
  Entity,
  Column,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  OneToMany,
} from 'typeorm';
// import { Photo } from '../photos/photo.entity';

@Entity('newuserdata')
export class Usersa {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { length: 50, nullable: true, name: 'first_name' })
  first_name: string;

  @Column('varchar', { length: 50, nullable: true, name: 'last_name' })
  last_name: string;

  @Column('varchar', { length: 50, nullable: true, name: 'email' })
  email: string;

  @Column('varchar', { length: 50, nullable: true, name: 'gender' })
  gender: string;

  @Column('varchar', { length: 20, nullable: true, name: 'ip_address' })
  ip_address: string;

  @Column('varchar', { length: 250, nullable: true, name: 'avatar' })
  avatar: string;

  

  // @Column('varchar', { length: 50, nullable: true, name: 'gender' })
  // gender: string;

  // @Column('varchar', { length: 50, nullable: true, name: 'email' })
  // email: string;

  // @Column('varchar', { length: 20, nullable: true, name: 'ip_address' })
  // ip_address: string;
}
